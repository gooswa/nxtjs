# nxt.js

#### A moduler based JS framework, take what you need and throw away the rest.



## JS Folder Breakdown

### core

**Core Modules that help build core functionality around certain core JS/DOM/Browser tasks**

+ **AnimationFrame.js** //System wide singleton to run a loop based render/update system per frame

+ **Application.js** //Can initialize a basic NXController for MVC based development