var path    = require('path');
var fs      = require('fs');



module.exports = function(grunt) {

    var DIR_DEPLOY      = path.join(__dirname, '..', '/public');
    var DIR_SOURCE      = path.join(__dirname, '..', '/src');

    grunt.initConfig({

        pkg : grunt.file.readJSON('package.json'),

        /*
        * Compiles any thing in the sass folder and makes a main.css for both source and deploy/public
        */
        sass : {
            /*
            * Distribution to deploy/public
            */
            dist :  {
                options: {
                    style: 'compressed'
                },
                files : [{
                    src: [DIR_SOURCE + '/css/sass/main.scss'],
                    dest: DIR_DEPLOY + '/css/main.css',
                    ext: '.css'
                }]
            },
            /*
            * Distribution to src
            */
            dev :  {
                options: {
                    style: 'expanded'
                },
                files : [{
                    src: [DIR_SOURCE + '/css/sass/main.scss'],
                    dest: DIR_SOURCE + '/css/main.css',
                    ext: '.css'
                }]
            }
        },

        /*
        * require.js optimizier
        */
        requirejs : {
            compile : {
                options : {
                    name: 'main',
                    baseUrl: DIR_SOURCE + '/js',
                    optimize: 'uglify2',
                    mainConfigFile: DIR_SOURCE + '/js/main.js',
                    out: DIR_DEPLOY + '/js/main.js',
                    paths: {
                        requireLib: DIR_SOURCE + '/js/vendor/require.js'
                    }
                }
            }
        },

        /*
        * Copies files from source directory to deploy/public directory
        */
        copy : {
            /*
            * Used for Require.js and Sass delopyment
            */
            main : {
                files : [
                    {
                        src: [DIR_SOURCE + '/*'],
                        dest: DIR_DEPLOY + '/',
                        filter: 'isFile',
                        expand: true,
                        flatten: true
                    },
                    {
                        cwd: DIR_SOURCE + '/img',
                        src: ['**'],
                        deset: DIR_DEPLOY + '/img',
                        expand: true
                    }
                ]
            },
            /*
            * Copy over require since it will be need in public/deploy directory
            */
            require : {
                files : [
                    {
                        src: [DIR_SOURCE + '/js/vendor/require.js'],
                        dest: DIR_DEPLOY + '/js/vendor/require.js'
                    }
                ]
            },
            /*
            * Make temp index file to process blocked scripts in basic JS script apps
            */
            temp : {
                files : [
                    {
                        src: [DIR_SOURCE + '/index.html'],
                        dest: DIR_SOURCE + '/index.tmp',
                    }
                ]
            },
            /*
            * Move it over to deploy/public folder
            */
            move : {
                files : [
                    {
                        src: [DIR_SOURCE + '/index.tmp'],
                        dest: DIR_DEPLOY + '/index.html',
                    }
                ]
            }
        },
        /*
        * Remove index.tmp file
        */
        clean: {
            options : {
                force : true
            },
            index: {
                src : [DIR_SOURCE + '/index.tmp']
            }
        },
        /*
        * Usemin for doing basic concat's of files in scipt blocks and css blocks
        */
        useminPrepare: {
            html : DIR_SOURCE + '/index.tmp',
            options: {
                dest : DIR_DEPLOY + '/'
            }
            
        },
        usemin: {
            html : DIR_SOURCE + '/index.tmp',
            options: {
                dest : DIR_DEPLOY + '/'
            }
        },

        /*
        * Watch Task for different types of build
        */
        watch : {
            rjscss: {
                files: [DIR_SOURCE + '/**/*'],
                tasks: ['requirejs', 'copy:main', 'sass']
            },
            blocks: {
                files: [DIR_SOURCE + '/**/*'],
                tasks: ['copy:main', 'copy:temp', 'useminPrepare', 'concat',
                        'uglify', 'cssmin', 'usemin', 'copy:move', 'clean:index']
            }

        }

    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-usemin');
   

    grunt.registerTask('watch-rjscss',  ['watch:rjscss']);
    grunt.registerTask('watch-block',   ['watch:blocks']);

    grunt.registerTask('build-rjscss',  ['requirejs', 'copy:main', 'sass', 'copy:require']);

    grunt.registerTask('build-block',   ['copy:main', 'copy:temp', 'useminPrepare', 'concat',
                                        'uglify', 'cssmin', 'usemin', 'copy:move', 'clean:index']);
    

};