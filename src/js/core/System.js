define(function (require, exports, module) {

    var _instance = null;

    var NXSystem = function() {
        if ( !(this instanceof NXSystem) ) {
            return new NXSystem();
        }

        this.mobile = false;
        this.phone = false;
        this.tablet = false;
        this.browser = {};
        this.css = {};
        this.media = {};
        this.graphics = {};

        this.agent = navigator.userAgent.toLowerCase();
        this.parseAgent();
    };

    function detect(agent, array) {
        if (typeof array === 'string') array = [array];
        for (var i = 0; i<array.length; i++) {
            if ( agent.indexOf(array[i]) != -1 ) return true;
        }
        return false;
    }

    NXSystem.prototype.parseAgent = function() {
        this.mobile = (!!('ontouchstart' in window));
        if (this.mobile) {
            this.mobile.tablet = window.innerWidth > 1000 || window.innerHeight > 900;
            this.mobile.phone = !this.mobile.tablet;
        }

        this.browser.chrome = detect(this.agent, 'chrome');
        this.browser.safari = !this.browser.chrome && detect(this.agent, 'safari');
        this.browser.opera  = !this.browser.safari && detect(this.agent, 'opr');
        this.browser.firefox = detect(this.agent, 'firefox');
        this.browser.ie = detect(this.agent, 'msie');
        this.browser.version = -1;

        if (this.browser.firefox) {
            this.browser.version = Number(this.agent.split('firefox/')[1].split('.')[0]);
            this.vendor = 'moz';
            this.css.vendor = 'Moz';
            this.css.tweenComplete = 'transitionend';
        } else if (this.browser.opera) {
            this.browser.version = Number(this.agent.split('opr/')[1].split('.')[0]);
            if (this.browser.version < 15) {
                this.vendor = 'o'; this.css.vendor = 'O'; this.css.tweenComplete = 'oTransitionEnd';
            } else {
                this.vendor = 'webkit'; this.css.vendor = 'Webkit'; this.css.tweenComplete = 'webkitTransitionEnd';
            }
        } else if (this.browser.ie) {
            this.browser.version = Number(this.agent.split('msie')[1].split('.')[0]);
            this.vendor = 'ms';
            this.css.vendor = 'ms';
            this.css.tweenComplete = 'msTransitionEnd';
        } else {
            this.browser.version = (this.browser.safari) ?
                                    Number(this.agent.split('version/')[1].split('.')[0].charAt(0))
                                    : Number(this.agent.split('chrome/')[1].split('.')[0]);
            this.vendor = 'webkit';
            this.css.vendor = 'Webkit';
            this.css.tweenComplete = 'webkitTransitionEnd';
        }

        var divStyle = document.createElement('div').style;
        this.css.transition     = ( (this.css.vendor+'Transition') in divStyle );
        this.css.transform      = ( (this.css.vendor+'Transform') in divStyle );
        this.css.perspective    = ( (this.css.vendor+'Perspective') in divStyle );

        if (!!document.createElement('audio').canPlayType) {
            this.media.audio = (this.browser.firefox || this.browser.opera) ? 'ogg' : 'mp3';
        } else {
            this.media.audio = false;
        }

        var vid = document.createElement('video');
        if (!!vid.canPlayType) {
            if (this.mobile) this.media.video = 'mp4';
            if (this.browser.chrome) this.media.video = 'webm';
            if (this.browser.firefox || this.browser.opera) {
                this.media.video =  (vid.canPlayType('video/webm; codecs="vorbis,vp8"')) ? 'webm' : 'ogv';
            }
            if (!!this.media.video) this.media.video = 'mp4';
        } else {
            this.media.video = false;
        }
    
        
        var canvas = document.createElement('canvas');
        this.graphics.canvas = canvas.getContext ? true : false;

        var wgl = canvas.getContext( 'experimental-webgl');
        this.graphics.webgl = (wgl !== null);   

        
    };

    return (_instance = ( _instance || NXSystem() ) );
});