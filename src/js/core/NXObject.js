define(function (require, exports, module) {

    var LinkedList = require('core/LinkedList');
    var system = require('core/System');

    
    /*
     * Core NXObject
     *
     */
    var NXObject = function(selector, type) {
        if (!(this instanceof NXObject)) {
            return new NXObject(selector, type);
        }
        this.parent = null;
        this._invalidate = false;
        this._x = 0;
        this._y = 0;
        this._z = 0;
        this._children = LinkedList();

        if (selector && typeof selector !== 'string') {
            this.el = selector; 
        }
        var existingElement = document.getElementById(selector);

        if (!existingElement) {
            this.type = type || 'div';
            this.el = document.createElement(this.type);
            this.el.id = selector;
        } else {
            this.el = existingElement;
        }
    };
    /*
     *
     * Getters/Setters
     *
     */
    Object.defineProperty(NXObject.prototype, 'visible', {
        get: function() {
            return (this.el.style.display === 'none') ? false : true;
        },
        set: function(value) {
            this.el.style.display = (value) ? 'block' : 'none';
        }
    });

    Object.defineProperty(NXObject.prototype, 'style', {
        get: function() {
            return this.el.style;
        }
    });

    Object.defineProperty(NXObject.prototype, 'text', {
        get: function() {
            return this.el.innerHTML;
        },
        set: function(value) {
            this.el.innerHTML = value;
        }
    });

    Object.defineProperty(NXObject.prototype, 'width', {
        get: function() {
            return this.el.style.width;
        },
        set: function(value) {
            this.el.style.width = value + 'px';
        }
    });

    Object.defineProperty(NXObject.prototype, 'height', {
        get: function() {
            return this.el.style.height;
        },
        set: function(value) {
            this.el.style.height = value + 'px';
        }
    });

    Object.defineProperty(NXObject.prototype, 'opacity', {
        get: function() {
            return this.el.style.opacity;
        },
        set: function(value) {
            this.el.style.opacity = value;
        }
    });

    Object.defineProperty(NXObject.prototype, 'x', {
        get: function() {
            return this._x;
        },
        set: function(value) {
            this._x = value;
            this.el.style[system.vendor+'Transform'] = 'translate3d('+ this._x +'px , '+ this._y +'px, 0)';
        }
    });

    Object.defineProperty(NXObject.prototype, 'y', {
        get: function() {
            return this._y;
        },
        set: function(value) {
            this._y = value;
            this.el.style[system.vendor+'Transform'] = 'translate3d('+ this._x +'px, '+ this._y +'px, 0)';
        }
    });


    NXObject.prototype.css = function(obj) {
        for (var type in obj) {
            var val = obj[type];
            if (!(typeof val === 'string' || typeof val === 'number')) continue;
            if (typeof val !== 'string' && type != 'opacity' && type != 'zIndex') val += 'px';
            this.el.style[type] = val;
        }
    };

    NXObject.prototype.transform = function(props) {
        var transforms = '';
        var translate = '';


        if (typeof props.x !== 'undefined' || typeof props.y !== 'undefined' || typeof props.z !== 'undefined') {
            var x = (props.x || 0);
            var y = (props.y || 0);
            var z = (props.z || 0);
            translate += x + 'px, ';
            translate += y + 'px';
            if (system.css.transform) {
                translate += ', ' + z + 'px';
                transforms += 'translate3d('+translate+')';
            } else {
                transforms += 'translate('+translate+')';
            }
        }
        
        if (typeof props.scale !== 'undefined') {
            transforms += 'scale('+props.scale+')';
        } else {
            if (typeof props.scaleX !== 'undefined') {
                transforms += 'scaleX('+props.scaleX+')';
            }
            if (typeof props.scaleY !== 'undefined') {
                transforms += 'scaleY('+props.scaleY+')';
            }
        }
        
        if (typeof props.rotation !== 'undefined') transforms += 'rotate('+props.rotation+'deg)';
        if (typeof props.rotateX !== 'undefined') transforms += 'rotateX('+props.rotateX+'deg)';
        if (typeof props.rotateY !== 'undefined') transforms += 'rotateY('+props.rotateY+'deg)';
        if (typeof props.rotateZ !== 'undefined') transforms += 'rotateZ('+props.rotateZ+'deg)';
        if (typeof props.skewX !== 'undefined') transforms += 'skewX('+props.skewX+'deg)';
        if (typeof props.skewY !== 'undefined') transforms += 'skewY('+props.skewY+'deg)';

        this.el.style[system.vendor+'Transform'] = transforms;
    };


    NXObject.prototype.addChild = function(nxObject) {
        if ( !(nxObject instanceof NXObject) ) throw "Must be NXObject to be added as child to a parent";
        nxObject.parent = this;
        this._children.push(nxObject);
        this.el.appendChild(nxObject.el);
    };


    NXObject.prototype.removeChild = function(nxObject, keep) {
        if ( !(nxObject instanceof NXObject) ) throw "Must be NXObject to be added as child to a parent";
        if (!keep) try { this.el.removeChild(nxObject.el); } catch(e) {};
        nxObject.parent = null;
        this._children.remove(nxObject);
    }


    window.nxt = NXObject;
    module.exports = NXObject;
});