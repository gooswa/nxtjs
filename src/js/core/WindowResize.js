define(function (require, exports, module) {

    var signals = require('signals');

    var WindowResize = {};
    WindowResize.updated = new signals.Signal();

    window.addEventListener('resize', function() {
        WindowResize.updated.dispatch();
    });

    module.exports = WindowResize;
});