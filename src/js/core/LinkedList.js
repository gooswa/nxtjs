define(function (require, exports, module) {

    var NXLinkedList = function() {
        if ( !(this instanceof NXLinkedList) ) {
            return new NXLinkedList();
        }
        this.length = 0;
        this.first = null;
        this.last = null;
        this.current = null;  
    };


    NXLinkedList.prototype.push = function(obj) {
        if (typeof obj !== 'object') throw 'LinkedList only accepts objects.';
        if (this.first === null) {
            obj.__prev = obj;
            obj.__next = obj;
            this.first = obj;
            this.last = obj;
        } else {
            obj.__prev = this.last;
            obj.__next = this.first;
            this.last.__next = obj;
            this.last = obj;
        }
        
        this.length++;
    };


    NXLinkedList.prototype.remove = function(obj) {
        if (typeof obj !== 'object') throw 'LinkedList only accepts objects.';
        if (this.length > 1) {
            obj.__prev.__next = obj.__next;
            obj.__next.__prev = obj.__prev;
            if (obj == this.first) this.first = obj.__next;
            if (obj == this.last) this.last = obj.__prev;
        } else {
            this.first = null;
            this.last = null;
        }
        
        obj.__prev = null;
        obj.__next = null;
        this.length--;
    };


    NXLinkedList.prototype.empty = function() {
        this.length = 0;
        this.first = null;
        this.last = null;
        this.current = null;
    };

    NXLinkedList.prototype.start = function() {
        this.current = this.first;
        return this.current;
    }
    
    NXLinkedList.prototype.next = function() {
        if (!this.current || !this.current.__next) return false;
        this.current = this.current.__next;
        if (this.current == this.current.__next || this.current == this.current.__prev) this.current = null;
        return this.current;
    }
    
    NXLinkedList.prototype.destroy = function() {
        return null;
    }
    
    module.exports = NXLinkedList;
});