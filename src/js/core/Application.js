define(function (require, exports, module) {

    var _instance   = null;
    var NXObject    = require('core/NXObject');
    var system      = require('core/System');
    var NXController = require('core/NXController');

    var Application = function() {
        if ( !(this instanceof Application) ) {
            return new Application();
        }
        this.stage = nxt('stage');
        this.system = system;
        this.rootController = null;
    };

    /*
     * Initialization Method for Application
     *
     */
    Application.prototype.init = function(controller) {
        if (!(controller instanceof NXController)) {
            throw "Can't initialize application without a subclass of NXController";
        }
        console.log('Application#init');
        
        this.rootController = controller;
        this.rootController.init();

        
    };

    /*
     * Singleton methods to grap application from anywhere
     *
     */
    return ( _instance = (_instance || Application()) );
});