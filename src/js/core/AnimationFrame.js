define(function (require, exports, module) {

    var signals = require('signals');

    var _id = null;

    window.requestAnimationFrame =  window.requestAnimationFrame || window.webkitRequestAnimationFrame ||
                                        window.mozRequestAnimationFrame || window.oRequestAnimationFrame ||
                                        window.msRequestAnimationFrame || function(  callback, element ) {
                                                                                _id = window.setTimeout( callback, 1000 / 60 );
                                                                            };
    window.cancelAnimationFrame =  window.cancelAnimationFrame || window.webkitCancelAnimationFrame ||
                                        window.mozCancelAnimationFrame || window.oCancelAnimationFrame ||
                                        window.msCancelAnimationFrame || function( _id  ) {
                                                                                window.clearTimeout(_id);
                                                                            };                                                             
    var AnimationFrame = {};
    AnimationFrame.updated = new signals.Signal();

    AnimationFrame.start = function() {
        _id = requestAnimationFrame(AnimationFrame.loop);
    };

    AnimationFrame.stop = function() {
        cancelAnimationFrame(_id);
    };

    AnimationFrame.loop = function() {
        AnimationFrame.updated.dispatch();
        _id = requestAnimationFrame(AnimationFrame.loop);
    }

    module.exports = AnimationFrame;

});