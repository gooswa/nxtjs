define(function (require, exports, module) {

    var NXObject = require('core/NXObject');
    /*
     * Core NXObject
     *
     */
    var ExtObject = function() {
        if (!(this instanceof ExtObject)) {
            return new ExtObject();
        }
        NXObject.call(this, "Extension");
    };

    ExtObject.prototype.__proto__ = NXObject.prototype;

    module.exports = ExtObject;
});