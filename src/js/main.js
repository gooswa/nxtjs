require.config({
    urlArgs: "bust=" + Date.now(),
    paths: {
        'signals' : 'vendor/signals',
        'TweenMax' : 'vendor/TweenMax.min',
        'TimelineMax' : 'vendor/TimelineMax.min'
    }
});



//Initialization for require.js

define(function (require, exports, module) {
    
    var app = require('core/Application');

    var MainController = require('MainController');
    console.log("======= MAIN START =======");
    app.init(MainController());
    
});