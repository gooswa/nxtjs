define(function (require, exports, module) {

    var app             = require('core/Application');

    var VideoPlayer     = require('media/VideoPlayer');
    var DisplayUtils    = require('util/DisplayUtils');
    var NXController    = require('core/NXController');
    var MouseWheel      = require('ui/MouseWheel');
    var AnimationFrame  = require('core/AnimationFrame');
    var WindowResize    = require('core/WindowResize');
    var TweenMax        = require('TweenMax');
    
    var velocity        = 0;
    var dampen          = 0.9;
    var speed           = 0.33;

    var dampID          = null;
    var startDampen     = false;

    
    var SEGEMENT_STATE_PLAY         = 0;
    var SEGEMENT_STATE_LOOP         = 1;
    var SEGEMENT_STATE_TRANSITION   = 2;

    var currentSegment  = 0;
    var segmentState    = SEGEMENT_STATE_PLAY;



    var videoPlayers    = [];

    var segments = [
        {
            video : 'video/FirstLoop_1.mp4',
            markInOut : {begin: 8.566667, end: 11.78},
            copy : 'Cajmere\'s intuitive design provides an immersive browsing experience.',
            copyShown : false
        },
        {
            video : 'video/SecondLoop_1.mp4',
            markInOut : {begin: 6.6, end: 10.7},
            copy : 'Every story is presented using the whole screen so you can give it your full attention.',
            copyShown : false
        },
        {
            video : 'video/ThirdLoop_1.mp4',
            markInOut : {begin: 5.7, end: 8.2},
            copy : 'Articles have a new that let you quickly see what\'s inside a post or article before opening.',
            copyShown : false
        }
    ];

    var MainController = function() {
        if ( !(this instanceof MainController) ) {
            return new MainController();
        }
    };
    MainController.prototype.__proto__ = NXController.prototype;



    MainController.prototype.init = function() {
        console.log('MainController#init');

        this.currentPlayer = null;

        this.createVideo();
        this.createCopy();

        this.mouse = MouseWheel(window);
        this.mouse.cancelEvent = true;
        this.mouse.updated.add(this.wheelUpdated, this);

        WindowResize.updated.add(this.windowResized, this);

        this.windowResized();

        AnimationFrame.updated.add(this.frameUpdated, this);
        

        var self = this;
        setTimeout(function() {
            self.startDemo();            
        }, 2000);
    };


    


    MainController.prototype.createCopy = function() {
        console.log("MainController#createCopy");
        this.copyBlock = nxt('copy-block');
        this.copyBlock.css({
            position: 'absolute',
            zIndex: 100
        });
        this.copyBlock.text = segments[currentSegment].copy;
        this.copyBlock.width = 450;
        this.copyBlock.height = 50;
        this.copyBlock.y = (window.innerHeight>>1) + 150;
        this.copyBlock.opacity = 0;
        this.copyBlock.visible = false;
        app.stage.addChild(this.copyBlock);
    };

    MainController.prototype.createVideo = function() {
        this.playerAlpha = VideoPlayer('demo-video-alpha', 1280, 720);
        this.playerAlpha.css({
            position: 'absolute',
            zIndex: 11
        });
        this.playerAlpha.autoplay = false;
        this.playerAlpha.loop = false;

        this.playerAlpha.ended.add(this.videoEnded, this);
        this.playerAlpha.load(segments[currentSegment].video);

        app.stage.addChild(this.playerAlpha);
        videoPlayers.push(this.playerAlpha);

        //Beta
        this.playerBeta = VideoPlayer('demo-video-beta', 1280, 720);
        this.playerBeta.css({
            position: 'absolute',
            zIndex: 12
        });
        this.playerBeta.visible = false;
        this.playerBeta.autoplay = false;
        this.playerBeta.loop = false;

        this.playerBeta.ended.add(this.videoEnded, this);
        this.playerBeta.load(segments[currentSegment+1].video);
        
        app.stage.addChild(this.playerBeta);
        videoPlayers.push(this.playerBeta);
        

        this.currentPlayer = this.playerAlpha;

    };

    MainController.prototype.wheelUpdated = function(delta, event) {
        velocity += delta * speed;
        startDampen = false;
        clearTimeout(dampID);
        dampID = setTimeout(function() {
            startDampen = true;
        }, 100);


    };

    MainController.prototype.windowResized = function() {
        var newRect = DisplayUtils.aspectFillAndCenter( this.playerAlpha.width, this.playerAlpha.height,
                                                        window.innerWidth, window.innerHeight);

        this.playerBeta.width = this.playerAlpha.width    = newRect.width;
        this.playerBeta.height = this.playerAlpha.height   = newRect.height;

        this.playerAlpha.transform({
            x: newRect.x,
            y: newRect.y
        });
        this.playerBeta.transform({
            x: newRect.x,
            y: newRect.y
        });

        this.copyBlock.x = (window.innerWidth>>1) + 150;
    };

    MainController.prototype.startDemo = function() {
        console.log("MainController#startDemo");
        AnimationFrame.start();
        this.currentPlayer.play();
    };

    MainController.prototype.frameUpdated = function() {
        if (startDampen) velocity *= dampen;
        if (Math.abs(velocity) < 0.001) velocity = 0;

        var markInOut = segments[currentSegment].markInOut;

        if ( segmentState <= SEGEMENT_STATE_LOOP && this.currentPlayer.currentTime >= markInOut.end ) {

            if (segmentState != SEGEMENT_STATE_LOOP) {
                segmentState = SEGEMENT_STATE_LOOP;
                this.showCopy();
            }
            this.currentPlayer.currentTime = markInOut.begin;
        }

        if (segmentState == SEGEMENT_STATE_LOOP && velocity <= -150) {
            segmentState = SEGEMENT_STATE_TRANSITION;
            velocity = 0;            
            this.hideCopy();
        }
        
        if (segmentState == SEGEMENT_STATE_LOOP && segments[currentSegment].copyShown) {
            this.copyBlock.y = Math.round((window.innerHeight>>1) + velocity);
            var opacityValue = Math.abs( ((window.innerHeight>>1) - this.copyBlock.y) / 100);
            this.copyBlock.opacity = Math.max(0, Math.min(1, 1-opacityValue));
        }
    };

    MainController.prototype.videoEnded = function(event) {
        console.log("MainController#onVideoEnded");
        currentSegment++;
        
        segmentState = SEGEMENT_STATE_PLAY;

        var realSegment = currentSegment%3;
        var pingPong = realSegment%2;
        this.copyBlock.text = segments[realSegment].copy;

        var prevPlayer = this.currentPlayer;
        this.currentPlayer = videoPlayers[pingPong];

        prevPlayer.visible = false;
        this.currentPlayer.visible = true;
        
        this.currentPlayer.play();

        prevPlayer.load(segments[currentSegment+1].video);
        
    };

    MainController.prototype.showCopy = function() {
        console.log("MainController#showCopy");
        console.log(this.currentPlayer.currentTime);
        var self = this;
        self.copyBlock.y = (window.innerHeight>>1) + 150;
        this.copyBlock.visible = true;
        TweenLite.to(this.copyBlock, 1,
        {   
            opacity: 1,
            y:(window.innerHeight>>1),
            ease:"Sine.easeOut",
            onComplete: function() {
                segments[currentSegment].copyShown = true;
            }
        });
    };

    MainController.prototype.hideCopy = function() {
        console.log("MainController#hideCopy");
        var self = this;
        TweenLite.to(this.copyBlock, 0.33,
        {   
            opacity: 0,
            y:0,
            onComplete: function() {
                self.copyBlock.visible = false;
                self.copyBlock.y = (window.innerHeight>>1) + 150;
                self.copyBlock.opacity = 0;
            }
        });
    };


    module.exports = MainController;

});