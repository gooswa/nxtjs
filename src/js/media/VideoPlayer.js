define(function (require, exports, module) {

    var NXObject = require('core/NXObject');
    var signals = require('signals');

    var VideoPlayer = function(id, width, height) {
        if (!(this instanceof VideoPlayer)) {
            return new VideoPlayer(id, width, height);
        }
        NXObject.call(this, id, 'video');
        this.width = width;
        this.height = height;

        this.isReady = false;

        this._listeners = {
            onMetaData          : this.onMetaData.bind(this),
            onLoadProgress      : this.onLoadProgress.bind(this),
            onCanPlayThrough    : this.onCanPlayThrough.bind(this),
            onError             : this.onError.bind(this),
            onEnded             : this.onEnded.bind(this)
        };
        /*
        this.metadata = new Signal();
        this.loadProgress = new Signal();
        this.canPlayThrough = new Signal();
        this.errored = new Signal();
        
        */
        this.ended = new signals.Signal();

        this.el.addEventListener('loadedmetadata', this._listeners.onMetaData);
        this.el.addEventListener('progress', this._listeners.onLoadProgress);
        this.el.addEventListener('canplaythrough', this._listeners.onCanPlayThrough);
        this.el.addEventListener('error', this._listeners.onError);
        this.el.addEventListener('ended', this._listeners.onEnded);
    };

    VideoPlayer.prototype.__proto__ = NXObject.prototype;


    Object.defineProperty(VideoPlayer.prototype, 'loop', {
        get: function() {
            return this.el.loop;
        },
        set: function(value) {
            this.el.loop = value;
        }
    });

    Object.defineProperty(VideoPlayer.prototype, 'currentTime', {
        get: function() {
            return this.el.currentTime;
        },
        set: function(value) {
            this.el.currentTime = value;
        }
    });

    Object.defineProperty(VideoPlayer.prototype, 'duration', {
        get: function() {
            return this.el.duration;
        }
    });

    Object.defineProperty(VideoPlayer.prototype, 'autoplay', {
        get: function() {
            return this.el.autoplay;
        },
        set: function(value) {
            this.el.autoplay = value;
        }
    });

    Object.defineProperty(VideoPlayer.prototype, 'preload', {
        get: function() {
            return this.el.preload;
        },
        set: function(value) {
            this.el.preload = value;
        }
    });

    Object.defineProperty(VideoPlayer.prototype, 'width', {
        get: function() {
            return this.el.width;
        },
        set: function(value) {
            this.el.width = value;
        }
    });

    Object.defineProperty(VideoPlayer.prototype, 'height', {
        get: function() {
            return this.el.height;
        },
        set: function(value) {
            this.el.height = value;
        }
    });


    VideoPlayer.prototype.load = function(videoSrc) {
        this.el.src = videoSrc;
    };

    VideoPlayer.prototype.play = function() {
        this.el.play();
    };

    VideoPlayer.prototype.onMetaData = function(event) {

        //console.log(event);
    };

    VideoPlayer.prototype.onLoadProgress = function(event) {
        //console.log(event);
    };

    VideoPlayer.prototype.onCanPlayThrough = function(event) {
        this.isReady = true;
        //console.log(event);
    };

    VideoPlayer.prototype.onError = function(event) {
        //console.log(event);
    };

    VideoPlayer.prototype.onEnded = function(event) {
       this.ended.dispatch();
    };

    module.exports = VideoPlayer;

});