define(function (require, exports, module) {

    var Matrix4 = function(mtx) {
        if (!(this instanceof Matrix4)) {
            return new Matrix4(mtx);
        };

        this.matrix = new Float32Array(16);
        this.identity();

        if (mtx instanceof Float32Array && mtx.length == 16) this.matrix = mtx;
        if (mtx instanceof Array && mtx.length == 16) this.matrix = new Float32Array(mtx);
        
    };


    Matrix4.prototype.identity = function() {
        var m = this.matrix;
        m[0] = 1;
        m[1] = 0;
        m[2] = 0;
        m[3] = 0;
        m[4] = 0;
        m[5] = 1;
        m[6] = 0;
        m[7] = 0;
        m[8] = 0;
        m[9] = 0;
        m[10] = 1;
        m[11] = 0;
        m[12] = 0;
        m[13] = 0;
        m[14] = 0;
        m[15] = 1;
    };

    Matrix4.prototype.transpose = function() {
        var m = this.matrix;
        var m01 = m[1], m02 = m[2], m03 = m[3], m12 = m[6], m13 = m[7], m23 = m[11];
        m[1] = m[4];
        m[2] = m[8];
        m[3] = m[12];
        m[4] = m01;
        m[6] = m[9];
        m[7] = m[13];
        m[8] = m02;
        m[9] = m12;
        m[11] = m[14];
        m[12] = m03;
        m[13] = m13;
        m[14] = m23;
    };


    Matrix4.prototype.invert = function() {
        var m = this.matrix;

        var m00 = m[0], m01 = m[1], m02 = m[2], m03 = m[3];
        var m10 = m[4], m11 = m[5], m12 = m[6], m13 = m[7];
        var m20 = m[8], m21 = m[9], m22 = m[10], m23 = m[11];
        var m30 = m[12], m31 = m[13], m32 = m[14], m33 = m[15];

        var b00 = m00 * m11 - m01 * m10,
            b01 = m00 * m12 - m02 * m10,
            b02 = m00 * m13 - m03 * m10,
            b03 = m01 * m12 - m02 * m11,
            b04 = m01 * m13 - m03 * m11,
            b05 = m02 * m13 - m03 * m12,
            b06 = m20 * m31 - m21 * m30,
            b07 = m20 * m32 - m22 * m30,
            b08 = m20 * m33 - m23 * m30,
            b09 = m21 * m32 - m22 * m31,
            b10 = m21 * m33 - m23 * m31,
            b11 = m22 * m33 - m23 * m32;

        det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

        if (!det) { 
            console.warn("Attempt to inverse a singular Matrix4. ", this.matrix);
            return null; 
        }
        det = 1.0 / det;

        m[0] = (m11 * b11 - m12 * b10 + m13 * b09) * det;
        m[1] = (m02 * b10 - m01 * b11 - m03 * b09) * det;
        m[2] = (m31 * b05 - m32 * b04 + m33 * b03) * det;
        m[3] = (m22 * b04 - m21 * b05 - m23 * b03) * det;
        m[4] = (m12 * b08 - m10 * b11 - m13 * b07) * det;
        m[5] = (m00 * b11 - m02 * b08 + m03 * b07) * det;
        m[6] = (m32 * b02 - m30 * b05 - m33 * b01) * det;
        m[7] = (m20 * b05 - m22 * b02 + m23 * b01) * det;
        m[8] = (m10 * b10 - m11 * b08 + m13 * b06) * det;
        m[9] = (m01 * b08 - m00 * b10 - m03 * b06) * det;
        m[10] = (m30 * b04 - m31 * b02 + m33 * b00) * det;
        m[11] = (m21 * b02 - m20 * b04 - m23 * b00) * det;
        m[12] = (m11 * b07 - m10 * b09 - m12 * b06) * det;
        m[13] = (m00 * b09 - m01 * b07 + m02 * b06) * det;
        m[14] = (m31 * b01 - m30 * b03 - m32 * b00) * det;
        m[15] = (m20 * b03 - m21 * b01 + m22 * b00) * det;

    };


    Matrix4.prototype.adjoint = function() {
        var m = this.matrix;
        var m00 = m[0], m01 = m[1], m02 = m[2], m03 = m[3];
        var m10 = m[4], m11 = m[5], m12 = m[6], m13 = m[7];
        var m20 = m[8], m21 = m[9], m22 = m[10], m23 = m[11];
        var m30 = m[12], m31 = m[13], m32 = m[14], m33 = m[15];

        m[0]  =  (m11 * (m22 * m33 - m23 * m32) - m21 * (m12 * m33 - m13 * m32) + m31 * (m12 * m23 - m13 * m22));
        m[1]  = -(m01 * (m22 * m33 - m23 * m32) - m21 * (m02 * m33 - m03 * m32) + m31 * (m02 * m23 - m03 * m22));
        m[2]  =  (m01 * (m12 * m33 - m13 * m32) - m11 * (m02 * m33 - m03 * m32) + m31 * (m02 * m13 - m03 * m12));
        m[3]  = -(m01 * (m12 * m23 - m13 * m22) - m11 * (m02 * m23 - m03 * m22) + m21 * (m02 * m13 - m03 * m12));
        m[4]  = -(m10 * (m22 * m33 - m23 * m32) - m20 * (m12 * m33 - m13 * m32) + m30 * (m12 * m23 - m13 * m22));
        m[5]  =  (m00 * (m22 * m33 - m23 * m32) - m20 * (m02 * m33 - m03 * m32) + m30 * (m02 * m23 - m03 * m22));
        m[6]  = -(m00 * (m12 * m33 - m13 * m32) - m10 * (m02 * m33 - m03 * m32) + m30 * (m02 * m13 - m03 * m12));
        m[7]  =  (m00 * (m12 * m23 - m13 * m22) - m10 * (m02 * m23 - m03 * m22) + m20 * (m02 * m13 - m03 * m12));
        m[8]  =  (m10 * (m21 * m33 - m23 * m31) - m20 * (m11 * m33 - m13 * m31) + m30 * (m11 * m23 - m13 * m21));
        m[9]  = -(m00 * (m21 * m33 - m23 * m31) - m20 * (m01 * m33 - m03 * m31) + m30 * (m01 * m23 - m03 * m21));
        m[10] =  (m00 * (m11 * m33 - m13 * m31) - m10 * (m01 * m33 - m03 * m31) + m30 * (m01 * m13 - m03 * m11));
        m[11] = -(m00 * (m11 * m23 - m13 * m21) - m10 * (m01 * m23 - m03 * m21) + m20 * (m01 * m13 - m03 * m11));
        m[12] = -(m10 * (m21 * m32 - m22 * m31) - m20 * (m11 * m32 - m12 * m31) + m30 * (m11 * m22 - m12 * m21));
        m[13] =  (m00 * (m21 * m32 - m22 * m31) - m20 * (m01 * m32 - m02 * m31) + m30 * (m01 * m22 - m02 * m21));
        m[14] = -(m00 * (m11 * m32 - m12 * m31) - m10 * (m01 * m32 - m02 * m31) + m30 * (m01 * m12 - m02 * m11));
        m[15] =  (m00 * (m11 * m22 - m12 * m21) - m10 * (m01 * m22 - m02 * m21) + m20 * (m01 * m12 - m02 * m11));
    };

    Matrix4.prototype.determinant = function() {
        var m = this.matrix;

        var m00 = m[0], m01 = m[1], m02 = m[2], m03 = m[3];
        var m10 = m[4], m11 = m[5], m12 = m[6], m13 = m[7];
        var m20 = m[8], m21 = m[9], m22 = m[10], m23 = m[11];
        var m30 = m[12], m31 = m[13], m32 = m[14], m33 = m[15];

        var b00 = m00 * m11 - m01 * m10,
            b01 = m00 * m12 - m02 * m10,
            b02 = m00 * m13 - m03 * m10,
            b03 = m01 * m12 - m02 * m11,
            b04 = m01 * m13 - m03 * m11,
            b05 = m02 * m13 - m03 * m12,
            b06 = m20 * m31 - m21 * m30,
            b07 = m20 * m32 - m22 * m30,
            b08 = m20 * m33 - m23 * m30,
            b09 = m21 * m32 - m22 * m31,
            b10 = m21 * m33 - m23 * m31,
            b11 = m22 * m33 - m23 * m32;

        // Calculate the determinant
        det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;
        return det;
    }

    Matrix4.prototype.multiply = function(bMatrix) {

        var be = bMatrix.matrix;
        var ae = this.matrix;

        var a11 = ae[0], a12 = ae[4], a13 = ae[8], a14 = ae[12];
        var a21 = ae[1], a22 = ae[5], a23 = ae[9], a24 = ae[13];
        var a31 = ae[2], a32 = ae[6], a33 = ae[10], a34 = ae[14];
        var a41 = ae[3], a42 = ae[7], a43 = ae[11], a44 = ae[15];

        var b11 = be[0], b12 = be[4], b13 = be[8], b14 = be[12];
        var b21 = be[1], b22 = be[5], b23 = be[9], b24 = be[13];
        var b31 = be[2], b32 = be[6], b33 = be[10], b34 = be[14];
        var b41 = be[3], b42 = be[7], b43 = be[11], b44 = be[15];

        ae[0] = a11 * b11 + a12 * b21 + a13 * b31 + a14 * b41;
        ae[4] = a11 * b12 + a12 * b22 + a13 * b32 + a14 * b42;
        ae[8] = a11 * b13 + a12 * b23 + a13 * b33 + a14 * b43;
        ae[12] = a11 * b14 + a12 * b24 + a13 * b34 + a14 * b44;

        ae[1] = a21 * b11 + a22 * b21 + a23 * b31 + a24 * b41;
        ae[5] = a21 * b12 + a22 * b22 + a23 * b32 + a24 * b42;
        ae[9] = a21 * b13 + a22 * b23 + a23 * b33 + a24 * b43;
        ae[13] = a21 * b14 + a22 * b24 + a23 * b34 + a24 * b44;

        ae[2] = a31 * b11 + a32 * b21 + a33 * b31 + a34 * b41;
        ae[6] = a31 * b12 + a32 * b22 + a33 * b32 + a34 * b42;
        ae[10] = a31 * b13 + a32 * b23 + a33 * b33 + a34 * b43;
        ae[14] = a31 * b14 + a32 * b24 + a33 * b34 + a34 * b44;

        ae[3] = a41 * b11 + a42 * b21 + a43 * b31 + a44 * b41;
        ae[7] = a41 * b12 + a42 * b22 + a43 * b32 + a44 * b42;
        ae[11] = a41 * b13 + a42 * b23 + a43 * b33 + a44 * b43;
        ae[15] = a41 * b14 + a42 * b24 + a43 * b34 + a44 * b44;

    };


    Matrix4.prototype.translate = function(vector) {
        var m = this.matrix;
        var x = vector[0], y = vector[1], z = vector[2];

        var m00 = m[0], m01 = m[1], m02 = m[2], m03 = m[3],
            m10 = m[4], m11 = m[5], m12 = m[6], m13 = m[7],
            m20 = m[8], m21 = m[9], m22 = m[10], m23 = m[11],
            m30 = m[12], m31 = m[13], m32 = m[14], m33 = m[15];
    
        m[0] = m00 + m03*x;
        m[1] = m01 + m03*y;
        m[2] = m02 + m03*z;
        m[3] = m03;

        m[4] = m10 + m13*x;
        m[5] = m11 + m13*y;
        m[6] = m12 + m13*z;
        m[7] = m13;

        m[8] = m20 + m23*x;
        m[9] = m21 + m23*y;
        m[10] = m22 + m23*z;
        m[11] = m23;
        m[12] = m30 + m33*x;
        m[13] = m31 + m33*y;
        m[14] = m32 + m33*z;
        m[15] = m33;
    };

    Matrix4.prototype.scale = function(vector) {
        var m = this.matrix;
        var x = vector[0], y = vector[1], z = vector[2];

        m[0] = m[0] * x;
        m[1] = m[1] * x;
        m[2] = m[2] * x;
        m[3] = m[3] * x;
        m[4] = m[4] * y;
        m[5] = m[5] * y;
        m[6] = m[6] * y;
        m[7] = m[7] * y;
        m[8] = m[8] * z;
        m[9] = m[9] * z;
        m[10] = m[10] * z;
        m[11] = m[11] * z;
        m[12] = m[12];
        m[13] = m[13];
        m[14] = m[14];
        m[15] = m[15];
    };

    Matrix4.prototype.rotate = function(x, y, z) {
        var sx = Math.sin(x);
        var cx = Math.cos(x);
        var sy = Math.sin(y);
        var cy = Math.cos(y);
        var sz = Math.sin(z);
        var cz = Math.cos(z);

        var xMatrix = Matrix4();
        var xm = xMatrix.matrix;
        xm[5] = cx;
        xm[6] = -sx;
        xm[9] = sx;
        xm[10] = cx;

        var yMatrix = Matrix4();
        var ym = yMatrix.matrix;
        ym[0] = cy;
        ym[2] = sy;
        ym[8] = -sy;
        ym[10] = cy;

        var zMatrix = Matrix4();
        var zm = zMatrix.matrix;
        zm[0] = cz;
        zm[1] = -sz;
        zm[4] = sz;
        zm[5] = cz;

        this.multiply(xMatrix);
        this.multiply(yMatrix);
        this.multiply(zMatrix);
    };
    

    Matrix4.prototype.rotateX = function(rad) {
        var m = this.matrix;
        var s = Math.sin(rad), c = Math.cos(rad);

        var m10 = m[4], m11 = m[5], m12 = m[6], m13 = m[7],
            m20 = m[8], m21 = m[9], m22 = m[10], m23 = m[11];

        m[4] = m10 * c + m20 * s;
        m[5] = m11 * c + m21 * s;
        m[6] = m12 * c + m22 * s;
        m[7] = m13 * c + m23 * s;
        m[8] = m20 * c - m10 * s;
        m[9] = m21 * c - m11 * s;
        m[10] = m22 * c - m12 * s;
        m[11] = m23 * c - m13 * s;
    };

    Matrix4.prototype.rotateY = function(rad) {
        var m = this.matrix;
        var s = Math.sin(rad), c = Math.cos(rad);

        var m00 = m[0], m01 = m[1], m02 = m[2], m03 = m[3],
            m20 = m[8], m21 = m[9], m22 = m[10], m23 = m[11];

        m[0] = m00 * c - m20 * s;
        m[1] = m01 * c - m21 * s;
        m[2] = m02 * c - m22 * s;
        m[3] = m03 * c - m23 * s;
        m[8] = m00 * s + m20 * c;
        m[9] = m01 * s + m21 * c;
        m[10] = m02 * s + m22 * c;
        m[11] = m03 * s + m23 * c;
    };

    Matrix4.prototype.rotateZ = function(rad) {
        var m = this.matrix;
        var s = Math.sin(rad), c = Math.cos(rad);

        var m00 = m[0], m01 = m[1], m02 = m[2], m03 = m[3],
            m10 = m[4], m11 = m[5], m12 = m[6], m13 = m[7];

        m[0] = m00 * c + m10 * s;
        m[1] = m01 * c + m11 * s;
        m[2] = m02 * c + m12 * s;
        m[3] = m03 * c + m13 * s;
        m[4] = m10 * c - m00 * s;
        m[5] = m11 * c - m01 * s;
        m[6] = m12 * c - m02 * s;
        m[7] = m13 * c - m03 * s;
    };

    Matrix4.prototype.frustum = function(left, right, bottom, top, near, far) {
        var m = this.matrix;
        var rl = 1 / (right - left), tb = 1 / (top - bottom), nf = 1 / (near - far);
        m[0] = (near * 2) * rl;
        m[1] = 0;
        m[2] = 0;
        m[3] = 0;
        m[4] = 0;
        m[5] = (near * 2) * tb;
        m[6] = 0;
        m[7] = 0;
        m[8] = (right + left) * rl;
        m[9] = (top + bottom) * tb;
        m[10] = (far + near) * nf;
        m[11] = -1;
        m[12] = 0;
        m[13] = 0;
        m[14] = (far * near * 2) * nf;
        m[15] = 0;
    };


    Matrix4.prototype.perspective = function(fov, aspect, near, far) {

        var m = this.matrix;
        var f = 1.0 / Math.tan(fov / 2), nf = 1 / (near - far);
        m[0] = f / aspect;
        m[1] = 0;
        m[2] = 0;
        m[3] = 0;
        m[4] = 0;
        m[5] = f;
        m[6] = 0;
        m[7] = 0;
        m[8] = 0;
        m[9] = 0;
        m[10] = (far + near) * nf;
        m[11] = -1;
        m[12] = 0;
        m[13] = 0;
        m[14] = (2 * far * near) * nf;
        m[15] = 0;
    };

    Matrix4.prototype.ortho = function(left, right, bottom, top, near, far) {
        var m = this.matrix;
        var lr = 1 / (left - right), bt = 1 / (bottom - top), nf = 1 / (near - far);
        m[0] = -2 * lr;
        m[1] = 0;
        m[2] = 0;
        m[3] = 0;
        m[4] = 0;
        m[5] = -2 * bt;
        m[6] = 0;
        m[7] = 0;
        m[8] = 0;
        m[9] = 0;
        m[10] = 2 * nf;
        m[11] = 0;
        m[12] = (left + right) * lr;
        m[13] = (top + bottom) * bt;
        m[14] = (far + near) * nf;
        m[15] = 1;
    };


    Matrix4.prototype.lookAt = function(eye, center, up) {
        var m = this.matrix;
        var x0, x1, x2, y0, y1, y2, z0, z1, z2, len,
        eyex = eye[0],
        eyey = eye[1],
        eyez = eye[2],
        upx = up[0],
        upy = up[1],
        upz = up[2],
        centerx = center[0],
        centery = center[1],
        centerz = center[2];

        if (Math.abs(eyex - centerx) < 0.0000001 &&
            Math.abs(eyey - centery) < 0.0000001 &&
            Math.abs(eyez - centerz) < 0.0000001) {
            this.identity();
            return;
        }

        z0 = eyex - centerx;
        z1 = eyey - centery;
        z2 = eyez - centerz;

        len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
        z0 *= len;
        z1 *= len;
        z2 *= len;

        x0 = upy * z2 - upz * z1;
        x1 = upz * z0 - upx * z2;
        x2 = upx * z1 - upy * z0;
        len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
        if (!len) {
            x0 = 0;
            x1 = 0;
            x2 = 0;
        } else {
            len = 1 / len;
            x0 *= len;
            x1 *= len;
            x2 *= len;
        }

        y0 = z1 * x2 - z2 * x1;
        y1 = z2 * x0 - z0 * x2;
        y2 = z0 * x1 - z1 * x0;

        len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
        if (!len) {
            y0 = 0;
            y1 = 0;
            y2 = 0;
        } else {
            len = 1 / len;
            y0 *= len;
            y1 *= len;
            y2 *= len;
        }

        m[0] = x0;
        m[1] = y0;
        m[2] = z0;
        m[3] = 0;
        m[4] = x1;
        m[5] = y1;
        m[6] = z1;
        m[7] = 0;
        m[8] = x2;
        m[9] = y2;
        m[10] = z2;
        m[11] = 0;
        m[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
        m[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
        m[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
        m[15] = 1;
    };

    Matrix4.prototype.cssMatrix = function() {
        var m = this.matrix;
        return 'matrix3d(' +
            m[0] + ',' +
            m[1] + ',' +
            m[2] + ',' +
            m[3] + ',' +

            m[4] + ',' +
            m[5] + ',' +
            m[6] + ',' +
            m[7] + ',' +

            m[8] + ',' +
            m[9] + ',' +
            m[10] + ',' +
            m[11] + ',' +

            m[12] + ',' +
            m[13] + ',' +
            m[14] + ',' +
            m[15] +
        ')';
    };

    return Matrix4;

});