define(function (require, exports, module) {

    var sys = require('core/System');
    var signals = require('signals');


    var MouseWheel = function(target) {
        if (!(this instanceof MouseWheel)) {
            return new MouseWheel(target);
        }
        this._target = target;
        this.updated = new signals.Signal();
        this.cancelEvent = false;
        this._divide = (sys.browser.firefox) ? 1 : (sys.browser.ie) ? 75 : 300;

        this._listeners = {
            string: (sys.browser.firefox) ? "wheel" : "mousewheel",
            wheelHandler : this.wheelHandler.bind(this)
        };
        this._target.addEventListener(this._listeners.string, this._listeners.wheelHandler );
    };



    MouseWheel.prototype.wheelHandler = function(event) {
        if (this.cancelEvent) {
            event.preventDefault();
            event.stopPropagation();
        }

        var delta = event.deltaY || event.wheelDelta;
        this.updated.dispatch(delta, event);

    };

    MouseWheel.prototype.destroy = function() {
        this.update.removeAll();
        this._target.removeEventListener(this._listeners.string, this._listeners.wheelHandler);
    };


    module.exports = MouseWheel;
});