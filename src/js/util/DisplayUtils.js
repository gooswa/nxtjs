define(function (require, exports, module) {

    var DisplayUtils = {};

    DisplayUtils.aspectFill = function(sourceWidth, sourceHeight, targetWidth, targetHeight) {
        var sourceRatio = sourceWidth / sourceHeight;
        var targetRatio = targetWidth / targetHeight;

        var scaleWidth = (sourceRatio <= targetRatio);
        var scalingFactor, scaledWidth, scaledHeight;
        if (scaleWidth) {
                scalingFactor = 1.0 / sourceRatio;
                scaledWidth = targetWidth;
                scaledHeight = Math.round(targetWidth * scalingFactor);
        } else {
                scalingFactor = sourceRatio;
                scaledWidth = Math.round(targetHeight * scalingFactor);
                scaledHeight = targetHeight;
        }
        return {width: scaledWidth,
                height: scaledHeight};
    };

    DisplayUtils.aspectFillAndCenter = function(sourceWidth, sourceHeight, targetWidth, targetHeight) {
        var newSize = DisplayUtils.aspectFill(sourceWidth, sourceHeight, targetWidth, targetHeight);
        var xPos = ((targetWidth>>1) - (newSize.width>>1));
        var yPos = ((targetHeight>>1) - (newSize.height>>1));

        return {width: newSize.width,
                height: newSize.height,
                x: xPos,
                y: yPos };
    };

    module.exports = DisplayUtils;

});